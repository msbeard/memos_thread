#include "thread.h"
#include "init.h"

#define DEBUG FALSE

void init_pool(void) {
    uint32 i;
    TCB thread;

    for (i = 1; i <= MAXTHREADS; i++) {
        thread.tid = i; // TID = 1...N
        thread.status = IDLE;
        thread.level = NORMAL;
        thread.stack = -1;

        pool [i % MAXTHREADS] = thread;
        if (DEBUG) {
            char str[20] = "Init thread ";
            terminal_writestring(str);
            char threadstr[10];
            itoa(threadstr, 'd', i);
            terminal_putchar('<');
            terminal_writestring(threadstr);
            terminal_putchar('>');
            terminal_putchar(' ');
        }
    }
}

void thread_restore(uint32 index) {
    __asm__ volatile ("mov %0, %%esp; // mov the next threads stack pointer into place\n"
            :
            : "r" (runqueue[index].stack)
            : "%esp"
            );
    __asm__("popa");
    return;
}

void thread_yield(uint32 thread_index) {

    __asm__("pusha");
    __asm__("\t movl %%esp,%0" : "=r"(runqueue[thread_index].stack));
    //toggle run state here so that the first time we end up here we 
    //are going to the next thread, but the second time (on restore) we 
    //finish the work.
    //after toggle if you were waiting, restore , if you were running, call next thread.
    //on a restore we will likely fall into schedule after the work is done??
    //
    //toggle
    if (runqueue[thread_index].status == RUNNING) {
        runqueue[thread_index].status = WAIT;

    } else if (runqueue[thread_index].status == WAIT) {
        runqueue[thread_index].status = RUNNING;
    }
    //act

    if (runqueue[thread_index].status == RUNNING) {
        thread_restore(thread_index);

    } else if (runqueue[thread_index].status == WAIT) {
        int next_thread_index = (thread_index + 1) % MAXTHREADS;
        if (runqueue[next_thread_index].stack != -1) {
            //restore next thread if it was suspended
            current_thread_id = next_thread_index;
            thread_restore(next_thread_index);

        } else {
            current_thread_id = next_thread_index;
            runqueue[next_thread_index].f();
        }
    }
}

uint32 thread_create(void * stack, void *func) {
    // Iterate over pool to find the first idle thread
    // Return TID of thread assigned work

    uint32 i, loc, id = 0;

    // Find the first non-busy thread and assign it work
    while (TRUE) {
        if (pool[i % MAXTHREADS].status == IDLE) {
            loc = i % MAXTHREADS;
            pool[loc].f = func;
            pool[loc].status = WAIT;
            id = pool[loc].tid;
            pool[loc].stack = -1;
            if (DEBUG) {
                char str[20] = "Create thread ";
                terminal_writestring(str);
                char threadstr[10];
                itoa(threadstr, 'd', id);
                terminal_putchar('<');
                terminal_writestring(threadstr);
                terminal_putchar('>');
                terminal_putchar(' ');
            }
            break;
        } else i++; //
    }
    return id;
}

// This demonstrates part 1 of Assignment #4

void func0(void) {

    for (uint32 i = 0; i < 10; i++) {
        char threadstr[10];
        itoa(threadstr, 'd', current_thread_id);
        terminal_putchar('<');
        terminal_writestring(threadstr);
        terminal_putchar('>');
        terminal_putchar(' ');

//        if (i == 5) {
//            thread_yield(current_thread_id);
//        }
    }

    return;
}

// This demonstrates part 2 of Assignment #4

void func1(uint32 id) {
    for (uint32 i = 0; i < 10; i++) {
        char threadstr[10];
        itoa(threadstr, 'd', id);
        terminal_putchar('<');
        terminal_writestring(threadstr);
        terminal_putchar('>');
        terminal_putchar(' ');
    }

    //    char sp[10];
    //    itoa(sp, 'd', runqueue[id].stack);
    //    terminal_putchar('|');
    //    terminal_writestring(sp);
    //    terminal_putchar('|');
    //    terminal_putchar(' ');

    return;
}

void func2(void) {
    static uint32 i = 0;
    i++;
    char *str1 = " Hello! Pre-yield on thread index ";
    terminal_writestring(str1);
    char thread_index[10];
    itoa(thread_index, 'd', current_thread_id);
    terminal_writestring(thread_index);
    thread_yield(current_thread_id);
    char *str2 = "finishing thread with id ";
    terminal_writestring(str2);
    itoa(thread_index, 'd', current_thread_id);
    terminal_writestring(thread_index);
    runqueue[current_thread_id].status = TERMINATED;
    runqueueSize--;
    return;
}

void schedule(void) {
    runqueueSize = 0;

    // Thread assigned to func0
    uint32 id;

    // Create N threads
    // Each thread is assigned to func0
    // The first available thread is assigned to the start
    // of the work queue
    for (uint32 i = 0; i < MAXTHREADS; i++) {
        id = thread_create(0, &func0); // 1...N

        if (DEBUG) {
            char str[20] = "Schedule thread ";
            terminal_writestring(str);
            char threadstr[10];
            itoa(threadstr, 'd', id);
            terminal_putchar('<');
            terminal_writestring(threadstr);
            terminal_putchar('>');
            terminal_putchar(' ');
        }
        // Assign thread to work queue
        runqueue[i] = pool[i];
        pool[i].status = ASSIGNED;
        runqueue[i].status = READY;

        if (DEBUG) {
            char str[20] = "Assign Thread ";
            terminal_writestring(str);
            char threadstr[10];
            itoa(threadstr, 'd', runqueue[i].tid);
            terminal_putchar('<');
            terminal_writestring(threadstr);
            terminal_putchar('>');
            terminal_putchar(' ');
        }
    }

    for (uint32 i = 0; i < MAXTHREADS; i++) {
        runqueue[i].status = RUNNING;
        if (DEBUG) {
            char str[20] = "Run thread ";
            terminal_writestring(str);
            char threadstr[10];
            itoa(threadstr, 'd', runqueue[0].tid);
            terminal_putchar('<');
            terminal_writestring(threadstr);
            terminal_putchar('>');
            terminal_putchar(' ');
        }
        current_thread_id = runqueue[i].tid;
        runqueue[i].f();
        // runqueue[id].status = TERMINATED;

        pool[i].status = IDLE;
        pool[i].level = NORMAL;
        pool[i].f = 0;
        pool[i].stack = -1;
    }

}