/* Authors: Michelle Beard and Calvin Fiegal
 */
#ifndef THREAD_HEADER_H
#define THREAD_HEADER_H

#include "types.h"

/* Maximum number of threads */
#define MAXTHREADS 3

/* Thread status */
enum state {
    NEW, // Thread being created...might not need
    READY, // Ready to be executed
    ASSIGNED, // Thread has been assigned work and is in work queue
    RUNNING, // Instructions being executed    
    IDLE, // Ready for work
    WAIT, // Waiting for event to occur
    TERMINATED // Thread has finished execution
};

/* Thread priority */
enum priority {
    HIGH,
    NORMAL,
    LOW
};

/* Thread datastructure */
typedef struct tcb_t {
    uint32 tid; // Thread ID
    enum state status; // State of Thread
    enum priority level; // Priority Level
    void (*f)(); // Function Pointer
    uint32 stack; // Stack pointer
} TCB;

/* Ready pool */
TCB pool [MAXTHREADS];

/* FIFO Queue */
TCB runqueue [MAXTHREADS];

/* Size of runqueue */
int runqueueSize;

/* Flag identifying whether or not a thread
 * in the pool has been assigned work.
 */

int stack1[1024];
int stack2[1024];
bool work [MAXTHREADS];

/* the id of the current running thread */
uint32 current_thread_id;

/* Binds a thread in the pool to a specific
 * stack and function address.
 * Returns the TID of the thread associated
 * with this call.
 * Loop through runqueue and assign the first 
 * idle thread work and return the TID of it. 
 */
uint32 thread_create(void *stack, void *func);

/* Initialize the pool with MAXTHREADS
 * Set each thread to IDLE and priority level NORMAL
 */
void init_pool(void);

/* Non-premptive schedule */
void schedule(void);

/* Premptive schedule */
void roundRobin(void);

// void yield(int thread_index);
void thread_restore(uint32 index);

/* 
 */
void thread_yield(uint32 indx);

/* Once a thread has finished execution, returned 
 * back to pool in IDLE status
 */
void thread_restore();

#endif